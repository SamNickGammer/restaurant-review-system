export const HotelList = [
    {
        name: 'Delicious Bites',
        location: '123 Main Street, Anytown, USA',
        averageRating: 3,
        ratings: [
            {
                user: 'John Smith',
                date: '2022-01-15',
                rating: 4,
                comments: 'Great food, but service was slow.'
            },
            {
                user: 'Jane Doe',
                date: '2022-02-01',
                rating: 5,
                comments: 'Amazing food and service!'
            }
        ]
    },
    {
        name: 'Taste of India',
        location: '456 Maple Avenue, Anytown, USA',
        averageRating: 2,
        ratings: [
            {
                user: 'Alice Johnson',
                date: '2022-01-02',
                rating: 5,
                comments: 'Best Indian food in town!'
            },
            {
                user: 'Bob Thompson',
                date: '2022-02-14',
                rating: 4,
                comments: 'Good food, but a bit pricey.'
            }
        ]
    },
    {
        name: 'Burgers & Fries',
        location: '789 Oak Street, Anytown, USA',
        averageRating: 4,
        ratings: [
            {
                user: 'Sarah Lee',
                date: '2022-01-05',
                rating: 4,
                comments: 'Great burgers, but fries were overcooked.'
            },
            {
                user: 'Tom Johnson',
                date: '2022-02-17',
                rating: 3,
                comments: 'Decent food, but not worth the price.'
            }
        ]
    },
    {
        name: 'Sushi Spot',
        location: '321 Pine Avenue, Anytown, USA',
        averageRating: 3,
        ratings: [
            {
                user: 'Kim Nguyen',
                date: '2022-01-10',
                rating: 5,
                comments: 'Amazing sushi and friendly staff!'
            },
            {
                user: 'Mike Johnson',
                date: '2022-02-22',
                rating: 4,
                comments: 'Fresh fish, but the rolls were small.'
            }
        ]
    },
    {
        name: 'Pizza Palace',
        location: '555 Elm Street, Anytown, USA',
        averageRating: 1,
        ratings: [
            {
                user: 'Emily Smith',
                date: '2022-01-18',
                rating: 4,
                comments: 'Delicious pizza, but service was slow.'
            },
            {
                user: 'Jason Kim',
                date: '2022-02-08',
                rating: 4,
                comments: 'Great crust, but the toppings were skimpy.'
            }
        ]
    },
    {
        name: 'Thai Terrace',
        location: '444 Oak Avenue, Anytown, USA',
        averageRating: 5,
        ratings: [
            {
                user: 'Sophie Lee',
                date: '2022-01-12',
                rating: 5,
                comments: 'Best Thai food around!'
            },
            {
                user: 'Benjamin Thompson',
                date: '2022-02-19',
                rating: 5,
                comments: 'Amazing curry and noodles!'
            }
        ]
    },
    {
        name: 'Steakhouse',
        location: '222 Main Street, Anytown, USA',
        averageRating: 3,
        ratings: [
            {
                user: 'Oliver Garcia',
                date: '2022-01-21',
                rating: 4,
                comments: 'Good steak, but sides were mediocre.'
            },
            {
                user: 'Ella Kim',
                date: '2022-02-12',
                rating: 4,
                comments: 'Tasty food, but a bit expensive.'
            }
        ]
    },
    {
        name: 'Mediterranean Grill',
        location: '777 Maple Street, Anytown, USA',
        averageRating: 5,
        ratings: [
            {
                user: 'Aiden Patel',
                date: '2022-01-08',
                rating: 5,
                comments: 'Amazing falafel and hummus!'
            },
            {
                user: 'Mia Jones',
                date: '2022-02-26',
                rating: 3,
                comments: 'Good food, but service could be better.'
            }
        ]
    },
]