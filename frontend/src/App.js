import React, { useEffect } from 'react'
import { BrowserRouter, Routes, Route } from "react-router-dom"
import "./Assets/scss/main.scss"
import LoginScreen from "./Screens/LoginScreen";
import SignUpScreen from "./Screens/SignUpScreen";
import NestedRouting from './NestedRouting';
import HotelDetails from './Screens/HotelDetails';

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/*" element={<NestedRouting />} />
        <Route path="/login" element={<LoginScreen />} />
        <Route path="/signup" element={<SignUpScreen />} />
        <Route path='/hotel/:id' element={<HotelDetails />} />
      </Routes>
    </BrowserRouter>

  );
}

export default App;
