import React, { useState } from 'react'
// import ListofTop from '../Components/HomeScreen/ListofTop'
import ImageSample from "../Assets/Images/login.jpg"
import StarRating from '../Components/StarRating'

import { HotelList } from '../data/HotelList'

import { useNavigate } from 'react-router-dom'

const HomeScreen = () => {

    const navigation = useNavigate()

    const [topFiveRate, setTopFiveRate] = useState([])
    React.useEffect(() => {
        const topFive = HotelList.sort((a, b) => b.averageRating - a.averageRating).slice(0, 5)
        setTopFiveRate(topFive)
        return () => {
            setTopFiveRate([])
        }
    }, [])

    return (
        <div className='homeScreen'>
            <h1>List of Top Restaurants</h1>
            <div className='listOfTopRes'>
                <div className="listOfTopRes__slides">
                    {
                        topFiveRate.map((item, index) => (
                            <div key={index} className="listOfTopRes__slides__box" onClick={() => { navigation(`/hotel/${index + 1}`) }}>
                                <img src={ImageSample} alt={`image${index}`} />
                                <div className="listOfTopRes__slides__box__content">
                                    <h3>{item.name}</h3>
                                    <p>{item.location}</p>
                                    <StarRating averageRating={item.averageRating} />
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
            {/* <ListofTop/> */}
        </div>
    )
}

export default HomeScreen