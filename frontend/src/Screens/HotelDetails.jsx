import React from 'react'
import { useParams } from 'react-router-dom'
import ImageSample from "../Assets/Images/login.jpg"

import { HotelList } from "../data/HotelList"
import StarRating from '../Components/StarRating'
import { Lorem } from '../data/JustData'

const HotelDetails = () => {
    const { id } = useParams()

    const hotel = HotelList[id - 1]


    return (
        <div className='hotel'>
            <div className='hotel__imgContainer'>
                <img src={ImageSample} alt={hotel.name} />
            </div>
            <div className='hotel__details'>
                <h1 className='hotel__name'>{hotel.name}</h1>
                <p className='hotel__location'>{hotel.location}</p>
                <div className='hotel__rating'>
                    <StarRating averageRating={hotel.averageRating} />
                </div>
            </div>
            <p className='hotel__description'>{Lorem}</p>
            <div className='hotel__comments'>
                <h2>Comments</h2>
                {hotel.ratings.map((rate) => (
                    <div>
                        <p>{rate.user}</p>
                        <p>{rate.date}</p>
                        <p>{rate.rating}</p>
                        <p>{rate.comments}</p>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default HotelDetails