import React from 'react'
import SignUpView from '../Components/SignUpScreen/SignUpView'

const SignUpScreen = () => {
    return (
        <div className='login'>
            <SignUpView />
        </div>
    )
}

export default SignUpScreen