import React from 'react'
import LoginView from '../Components/LoginScreen/LoginView'

const LoginScreen = () => {

    return (
        <div className='login'>
            <LoginView />
        </div>
    )
}

export default LoginScreen