import React, { useEffect } from 'react'
import { Route, Routes } from 'react-router-dom'
import HomeScreen from './Screens/HomeScreen'
import AboutScreen from './Screens/AboutScreen'
import NavBar from './Components/NavBar'

const NestedRouting = () => {

    // const userVerification = (userData) => {
    //     return userData === "samnick"
    // }

    // useEffect(() => {
    //     const user = localStorage.getItem('user');
    //     if (user && !userVerification(user)) {
    //         window.location.href = '/login';
    //     }

    // }, [])

    return (
        <>
            <NavBar />
            <div style={{ height: "calc(100vh - 60px)", marginTop: "60px", overflowY: "auto" }}>
                <Routes>
                    <Route path="/" element={<HomeScreen />} />
                    <Route path="about" element={<AboutScreen />} />
                </Routes>
            </div>
        </>
    )
}

export default NestedRouting