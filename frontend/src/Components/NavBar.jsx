import React from 'react'
import Logo from "../Assets/Logo/HostStarFullLogo_Black.png"
import Sam from "../Assets/Images/ImageProfile_Squre_black_VeryLowRes_L200.png"

const loginStatus = true;

const NavBar = () => {
    return (
        <div className='navbar'>
            <div className='navbar__logo'>
                <img src={Logo} alt="logo" />
            </div>
            <div className='navbar__menu'>
                <div className='navbar__menu__searchBar'>
                    <input type="text" placeholder='Search...' />
                </div>
                <div className='navbar__menu__icons'>
                    {loginStatus ? (
                        <div className='navbar__menu__icons__user'>
                            <img src={Sam} alt="" />
                        </div>
                    ) : (
                        <div className='navbar__menu__icons__button'>Login</div>
                    )}
                </div>
            </div>
        </div>
    )
}

export default NavBar