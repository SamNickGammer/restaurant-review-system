import React from 'react'
import LoginImageAssets from "../../Assets/Images/login.jpg"
import LogoFullBlack from "../../Assets/Logo/HostStarFullLogo_Black.png"
import { Link } from "react-router-dom"

const LoginView = () => {
    return (
        <div className='login__container'>
            <div className='login__image-container'>
                <img src={LoginImageAssets} alt="login" />
            </div>
            <div className='login__text-container'>
                <div className='login__text-container__brand-wrapper'>
                    <img src={LogoFullBlack} alt="logo" height={100} />
                </div>
                <div className='login__text-container__title'>Sign into your account</div>
                <div className='login__text-container__form'>
                    <div className='login__text-container__form__input'>
                        <label htmlFor="email">Email</label>
                        <input type="text" placeholder="Email" name='email' autoComplete='off' />
                    </div>
                    <div className='login__text-container__form__input'>
                        <label htmlFor="password">Password</label>
                        <input type="password" placeholder="Password" name="password" />
                    </div>
                    <div className='login__text-container__form__input'>
                        <button>Sign In</button>
                    </div>
                    <div className="login__text-container__form__signIn">
                        <div className='login__text-container__form__signIn--b'>
                            <p>Don't have an account? <Link to={"/signup"}>Sign Up</Link></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginView