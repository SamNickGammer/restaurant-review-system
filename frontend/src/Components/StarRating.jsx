import React, { useState } from "react";

const StarRating = ({ averageRating }) => {
  const [rating, setRating] = useState(averageRating || 0);
  const [hover, setHover] = useState(0);


  return (
    <div className="star-rating">
      {[...Array(5)].map((star, index) => {
        index += 1;
        return (
          !averageRating ? (
            <button
              type="button"
              key={index}
              className={index <= (hover || rating) ? "star-rating__on" : "star-rating__off"}
              onClick={() => setRating(index)}
              onMouseEnter={() => setHover(index)}
              onMouseLeave={() => setHover(rating)}
              style={{ cursor: "pointer" }}
            >
              <span className="star-rating__star">&#9733;</span>
            </button>

          ) : (
            <button
              type="button"
              key={index}
              className={index <= (hover || rating) ? "star-rating__on" : "star-rating__off"}
            >
              <span className="star-rating__star">&#9733;</span>
            </button>
          )
        );
      })}
    </div>
  );
};

export default StarRating;