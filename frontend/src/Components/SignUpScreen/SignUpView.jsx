import React from 'react'
import LoginImageAssets from "../../Assets/Images/login.jpg"
import LogoFullBlack from "../../Assets/Logo/HostStarFullLogo_Black.png"
import { Link } from "react-router-dom"

const SignUpView = () => {

  // const [email, setEmail] = React.useState("")
  // const [password, setPassword] = React.useState("")
  // const [cnfPassword, setCnfPassword] = React.useState("")
  // const [error, setError] = React.useState(false)

  // // const errorToggler = () => {
  // //   const input = document.querySelectorAll(".login__text-container__form__input--error")
  // //   input.forEach((item) => {
  // //     item.classList.toggle("login__text-container__form__input--error--active")
  // //   })
  // // }

  // const confirmPassword = (e) => {
  //   if (password !== "") {
  //     if (e.currentTarget.value === password) {
  //       setCnfPassword(e.currentTarget.value)
  //       setError(false)
  //     }
  //     else {
  //       setError(true)
  //     }
  //     if (e.currentTarget.value === "") {
  //       setError(false)
  //     }
  //   }
  // }

  const submitSignIn = (e) => {
    e.preventDefault()
    // if (email !== "" && password !== "" && cnfPassword !== "") {
    //   if (password === cnfPassword) {
    //     console.log("success", email, password, cnfPassword)
    //   }
    // }
  }

  return (
    <div className='login__container'>
      <div className='login__image-container'>
        <img src={LoginImageAssets} alt="login" />
      </div>
      <div className='login__text-container'>
        <div className='login__text-container__brand-wrapper'>
          <img src={LogoFullBlack} alt="logo" height={100} />
        </div>
        <div className='login__text-container__title'>Create a new account</div>
        <form className='login__text-container__form'>
          <div className='login__text-container__form__input'>
            <label htmlFor="email">Email</label>
            <input type="email" placeholder="Email" name='email' autoComplete='off' required />
          </div>
          <div className='login__text-container__form__input'>
            <label htmlFor="password">Password</label>
            <input type="password" placeholder="Password" name="password" required />
          </div>
          <div className='login__text-container__form__input'>
            <label htmlFor="cnfPassword">Confirm Password</label>
            <input type="password" placeholder="Password" name="cnfPassword" required />
          </div>
          <div className='login__text-container__form__input'>
            <button onClick={submitSignIn}>Sign In</button>
          </div>
          <div className="login__text-container__form__signIn">
            <div className='login__text-container__form__signIn--b'>
              <p>Already have account? <Link to={"/login"}>Login</Link></p>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}

export default SignUpView