import React, { useEffect } from 'react'
import { SignedInRoute, SignedOutRoute } from './Navigation'

const AuthNavigation = () => {
    const [currentUser, setCurrentUser] = React.useState(null)

    useEffect(() => {
        //Todo: Get Data From User Last Time Login API (In Future)
        // const user = JSON.parse(localStorage.getItem('user'))
        const user = "Sam"
        setCurrentUser(user)
    }, [])

    return (
        <div>
            {currentUser ? (<SignedInRoute />) : (<SignedOutRoute />)}
        </div>
    )
}

export default AuthNavigation