import React, { useEffect } from 'react'
import { Routes, Route, BrowserRouter } from "react-router-dom"
import HomeScreen from './Screens/HomeScreen'
import LoginScreen from './Screens/LoginScreen'
import AboutScreen from './Screens/AboutScreen'
import SignUpScreen from './Screens/SignUpScreen'


export const SignedInRoute = () => {
    return (
        <BrowserRouter>
            <Routes location={"/"}>
                <Route path="/" element={<HomeScreen />} />
            </Routes>
            <Routes>
                <Route path="/about" element={<AboutScreen />} />

            </Routes>
        </BrowserRouter>
    )
}

export const SignedOutRoute = () => {

    return (
        <BrowserRouter >
            <Routes location={"/login"}>
                <Route path="/login" element={<LoginScreen />} />
                <Route path="/signup" element={<SignUpScreen />} />
            </Routes>
        </BrowserRouter>
    )
}