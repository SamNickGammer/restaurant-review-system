import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';

@Controller("api")
export class AppController {
  constructor(private readonly authService: AuthService) { }

  @Post("user/login")
  @UseGuards(AuthGuard("local"))
  login(@Request() req): string {
    return this.authService.generateToken({ ...req.user })
  }

  @Post("admin/login")
  @UseGuards(AuthGuard("local"))
  loginAdmin(@Request() req): string {
    return this.authService.generateToken({ ...req.user })
  }

}