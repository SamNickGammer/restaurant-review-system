import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/users.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user/typeorm/entities/User';
import { RestaurantModule } from './restaurant/restaurant.module';
import { AdminModule } from './admin/admin.module';
import { Admin } from './admin/entity/Admin';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '57489Raj',
    database: 'finalproject',
    entities: [User, Admin],
    synchronize: true,
  }), UserModule, AdminModule, RestaurantModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
