import { Injectable } from "@nestjs/common"
import { PassportStrategy } from "@nestjs/passport"
import { ExtractJwt, Strategy } from "passport-jwt"

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: "key"
        })
    }

    async validate(payload: any) {
        return payload
    }

    // async validate(username: string, password: string) {
    //     const user = await this.userService.validateUser(username, password)
    //     if (!user) {
    //         throw new Error("User not found")
    //     }
    //     return user
    // }
}