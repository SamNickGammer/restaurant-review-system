import { AdminModule } from './../admin/admin.module';
import { JWTStrategy } from './jwt.strategy.service';
import { AuthService } from 'src/auth/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { UserStrategy } from './user.strategy.service';
import { Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { UserModule } from '../user/users.module';
import { AdminStrategy } from './admin.strategy.service';

@Module({
    imports: [PassportModule, UserModule, AdminModule, JwtModule.register({
        secret: "key",
        signOptions: {
            expiresIn: "12h"
        }
    })],
    controllers: [],
    providers: [UserStrategy, JWTStrategy, AuthService],
    exports: [AuthService]
})

export class AuthModule { } 