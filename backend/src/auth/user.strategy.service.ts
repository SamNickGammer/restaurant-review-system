import { UnauthorizedException, Injectable } from "@nestjs/common"
import { PassportStrategy } from "@nestjs/passport"
import { Strategy } from "passport-local"
import { UserService } from "src/user/services/user/user.service"

@Injectable()
export class UserStrategy extends PassportStrategy(Strategy) {
    constructor(private userService: UserService) {
        super()
    }

    async validate(username: string, password: string) {
        const user = await this.userService.getUserByUserName(username)
        // console.log(user, username, password)
        if (user === undefined) throw new UnauthorizedException();
        if (user !== undefined && user.password === password) {
            return user
        } else {
            throw new UnauthorizedException();
        }
    }
}