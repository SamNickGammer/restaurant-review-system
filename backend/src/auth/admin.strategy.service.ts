import { AdminService } from './../admin/admin.service';
import { UnauthorizedException, Injectable } from "@nestjs/common"
import { PassportStrategy } from "@nestjs/passport"
import { Strategy } from "passport-local"

@Injectable()
export class AdminStrategy extends PassportStrategy(Strategy) {
    constructor(private adminService: AdminService) {
        super()
    }

    async validate(username: string, password: string) {
        const admin = await this.adminService.getAdminByUserName(username)
        // console.log(user, username, password)
        if (admin === undefined) throw new UnauthorizedException();
        if (admin !== undefined && admin.password === password) {
            return admin
        } else {
            throw new UnauthorizedException();
        }
    }
}