import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, forwardRef } from "@nestjs/common";

@Module({
    imports: [TypeOrmModule.forFeature([])],
    controllers: [],
    providers: []
})

export class RestaurantModule { }