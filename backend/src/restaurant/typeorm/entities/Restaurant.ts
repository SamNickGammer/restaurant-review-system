import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity({ name: 'restaurant' })
export class User {
    @PrimaryGeneratedColumn({ type: "bigint" })
    id: number;

    @Column({ unique: true })
    username: string

    @Column()
    password: string;

    @Column()
    name: string

    @Column({ default: "user" })
    role: "admin" | "user"
}