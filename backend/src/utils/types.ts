export type CreateUserParam = {
    username: string
    password: string
    name: string
    // isAdmin: boolean
}


export type CreateAdminParam = {
    username: string
    password: string
    name: string
    // isAdmin: boolean
}

export type UserDetails = {
    id: number
    username: string
    password: string
    name: string
    isAdmin: boolean
}