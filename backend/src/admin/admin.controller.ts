import { CONSTRAINS } from './../utils/constrains';
import { RoleGuard } from './../auth/role.guard';
import { Body, Controller, Get, Post, UseGuards } from "@nestjs/common";
import { AdminService } from "./admin.service";
import { AuthGuard } from "@nestjs/passport";
import { JoiValidationPipes } from 'src/validation/joi-validation.pipe';
import { createAdminSchema } from './schema/createAdmin.schema';
import { CreateAdminDTO } from './dtos/CreateAdmin.dto';

@Controller("api/admin")
export class AdminController {
    constructor(
        private adminService: AdminService
    ) { }

    @Get()
    @UseGuards(AuthGuard("jwt"), new RoleGuard(CONSTRAINS.ROLES.ADMIN))
    getAdmins() {
        return this.adminService.getAdmins()
    }

    @Post("create")
    // @UseGuards(AuthGuard("jwt"), new RoleGuard(CONSTRAINS.ROLES.ADMIN))
    async createAdmin(
        @Body(new JoiValidationPipes(createAdminSchema)) CreateAdminDTO: CreateAdminDTO
    ) {
        const createdAdmin = await this.adminService.createAdmin(CreateAdminDTO)
        return {
            message: "Admin created successfully",
            id: createdAdmin.id,
            username: createdAdmin.username
        }
    }

}