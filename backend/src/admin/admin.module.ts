import { AuthModule } from './../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, forwardRef } from "@nestjs/common";
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { Admin } from './entity/Admin';

@Module({
    imports: [TypeOrmModule.forFeature([Admin]), forwardRef(() => AuthModule)],
    controllers: [AdminController],
    providers: [AdminService],
    exports: [AdminService]
})

export class AdminModule { }