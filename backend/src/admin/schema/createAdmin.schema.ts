import * as joi from "joi"

export const createAdminSchema = joi.object({
    username: joi.string().required(),
    password: joi.string().required(),
    name: joi.string().required(),
    //role be either admin or user
    role: joi.string().default("admin")
})