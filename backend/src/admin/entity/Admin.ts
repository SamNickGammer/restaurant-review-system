import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity({ name: 'admin' })
export class Admin {
    @PrimaryGeneratedColumn({ type: "bigint" })
    id: number;

    @Column({ unique: true })
    username: string

    @Column()
    password: string;

    @Column()
    name: string

    @Column()
    role: string
}