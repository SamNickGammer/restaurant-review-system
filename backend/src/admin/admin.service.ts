import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException } from "@nestjs/common";
import { Admin } from './entity/Admin';
import { Repository } from 'typeorm';
import { CreateAdminParam } from 'src/utils/types';


@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(Admin) private adminRepository: Repository<Admin>
    ) { }

    getAdmins() {
        return this.adminRepository.find()
    }

    async createAdmin(createAdminDetails: CreateAdminParam) {
        const createdAdmin = this.adminRepository.create({
            username: createAdminDetails.username,
            password: createAdminDetails.password,
            name: createAdminDetails.name,
            role: "admin"
        })
        return this.adminRepository.save(createdAdmin)
    }



    //Function to validate wheter it is admin or not.

    async getAdminByUserName(username: string): Promise<Admin> {
        const admin = await this.findOneByUserName(username)
        return admin
    }

    private async findOneByUserName(username: string) {
        let admin;
        try {
            admin = await this.adminRepository.findOneBy({ username: username })
        } catch (err) {
            throw new NotFoundException("Admin Not Found")
        }
        if (!admin) {
            throw new NotFoundException("Admin Not Found")
        }
        return admin
    }


}