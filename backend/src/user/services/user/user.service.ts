import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm"
import { User } from 'src/user/typeorm/entities/User';
import { CreateUserParam } from 'src/utils/types';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private userRepository: Repository<User>
    ) { }


    getUsers() {
        return this.userRepository.find()
    }

    async createUser(createUserDetails: CreateUserParam) {

        const createdUser = this.userRepository.create({
            username: createUserDetails.username,
            password: createUserDetails.password,
            name: createUserDetails.name,
            role: "user"
        })
        return this.userRepository.save(createdUser)
    }

    async getUserByUserName(username: string): Promise<User> {
        // const user = this.userRepository.createQueryBuilder('user')
        //     .where('user.username = :username', { username })
        //     .getOne();
        const user = await this.findOneByUserName(username)
        return user
    }

    private async findOneByUserName(username: string) {
        let user;
        try {
            user = await this.userRepository.findOneBy({ username: username })
        } catch (err) {
            throw new NotFoundException("User Not Found")
        }
        if (!user) {
            throw new NotFoundException("User Not Found")
        }
        return user
    }

}
