import { AuthModule } from './../auth/auth.module';
import { UserService } from './services/user/user.service';
import { UserController } from './controller/user/user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, forwardRef } from "@nestjs/common";
import { User } from 'src/user/typeorm/entities/User';

@Module({
    imports: [TypeOrmModule.forFeature([User]), forwardRef(() => AuthModule)],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})

export class UserModule { }