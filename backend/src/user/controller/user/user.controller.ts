import { CONSTRAINS } from './../../../utils/constrains';
import { CreateUserDTO } from 'src/user/dtos/CreateUser.dto';
import { UserService } from '../../services/user/user.service';
import { Controller, Get, Post, Body, Param, UseGuards, Request } from '@nestjs/common';
import { JoiValidationPipes } from 'src/validation/joi-validation.pipe';
import { createUserSchema } from 'src/user/schema/createUser.schema';
import { AuthGuard } from "@nestjs/passport"
import { RoleGuard } from 'src/auth/role.guard';

@Controller('api/users')
export class UserController {
    constructor(
        private userService: UserService,
    ) { }

    //For Admin
    @Get()
    @UseGuards(AuthGuard("jwt"), new RoleGuard(CONSTRAINS.ROLES.ADMIN))
    getUsers() {
        return this.userService.getUsers()
    }

    @Post("create")
    async createUser(@Body(new JoiValidationPipes(createUserSchema)) CreateUserDTO: CreateUserDTO) {
        const createdUser = await this.userService.createUser(CreateUserDTO)
        return {
            message: "User created successfully",
            id: createdUser.id,
            username: createdUser.username
        }
    }

    // //Tesing 
    // @Get(":username")
    // async getUserByUserName(
    //     @Param("username") username: string
    // ) {
    //     const user = await this.userService.getUserByUserName(username)
    //     return {
    //         message: "User found",
    //         user
    //     }
    // }

    //For Users
    @Get("profile")
    @UseGuards(AuthGuard("jwt"))
    async getProfile(@Request() req) {
        const user = await this.userService.getUserByUserName(req.user.username)
        return {
            ...user
        }
    }



}
